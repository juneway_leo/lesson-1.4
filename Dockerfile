from debian:10 as build
ARG VER
RUN apt update && apt install -y make gcc wget libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev dpkg-dev
ADD http://nginx.org/download/nginx-$VER.tar.gz /opt 
WORKDIR /opt
RUN tar xvfz nginx-$VER.tar.gz && cd nginx-1.20.1 && ./configure && make && make install 

FROM debian:10 
WORKDIR /usr/local/nginx
COPY --from=build /usr/local/nginx/ .
RUN chmod +x sbin/nginx && ln -sf /home/nginx.conf conf/nginx.conf
CMD ["sbin/nginx", "-g", "daemon off;"]
